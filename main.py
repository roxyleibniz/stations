import sqlalchemy
from flask import Flask, request, Response
from werkzeug.utils import secure_filename


from db import db_init
from models import Img

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///img.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db_init(app)


@app.route('/upload', methods=['POST'])
def upload():
    pic = request.files['pic']
    if not pic:
        return 'No pic uploaded!', 400

    filename = secure_filename(pic.filename)
    mimetype = pic.mimetype
    if not filename or not mimetype:
        return 'Bad upload!', 400

    img = Img(img=pic.read(), name=filename, mimetype=mimetype)
    db.session.add(img)
    db.session.commit()

    return 'Img Uploaded!', 200


@app.route('/<int:stationcode>')
def get_img(stationcode):
    img = Img.query.filter_by(stationcode=stationcode).first()
    if not img:
        return 'Изображение не найдено', 404

    return Response(img.img, imagefile=img.imagefile)
