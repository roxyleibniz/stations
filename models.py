from db import db_init


class Station(db.Model):
    imageid = db.Column(db.Integer,nullable=False)
    stationcode = db.Column(db.Text, unique=True, nullable=False, primary_key=True)
    imagefile = db.Column(db.Text, nullable=False)
    imagethumb = db.Column(db.Text, nullable=False)
    rectime = db.Column(db.datetime,nullable=False)
